from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from levart.services.travel import TravelService


class TravelView(APIView):

    def post(self, request, *args, **kwargs):
        try:
            origin = request.data["origin"]["value"]
            category = request.data["category"]
            budget = int(request.data["budget"])
            response_data = TravelService().shortest_path(origin, category, budget)
            print(response_data)
            return Response(response_data, status=status.HTTP_200_OK)
        except Exception as e:
            import traceback
            traceback.print_exc()
            return Response({'reason': str(e)}, status=status.HTTP_400_BAD_REQUEST)
