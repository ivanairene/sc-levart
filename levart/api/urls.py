from django.urls import re_path

from levart.api.views import (
    travel
)

urlpatterns = [
    re_path(r'^travel/$',
            travel.TravelView.as_view(),
            name='travel-path'),
]
