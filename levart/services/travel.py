from levart.utils.loading import get_model


Edge = get_model('edge', 'Edge')
City = get_model('city', 'City')


class TravelService(object):
    def get_all_edges(self, category):
        edges = []
        cities = None
        if category == "beach":
            cities = City.objects.filter(beach_view=True)
        if category == "mountain":
            cities = City.objects.filter(mountain_view=True)
        if category == "city":
            cities = City.objects.filter(city_view=True)
        if category == "culinary":
            cities = City.objects.filter(culinary=True)
        cities = [city.id for city in cities]
        for edge in Edge.objects.filter(destination__id__in=cities):
            edges.append({
                "origin": edge.source.name,
                "destination": edge.destination.name,
                "method": edge.method,
                "ticket_price": edge.ticket_price,
                "ticket_page_url": edge.ticket_page_url
            })
        return edges

    def heuristic(self, edge, edges):
        if not edges:
            return edge["ticket_price"]
        for e in edges:
            if (e["destination"] == edge["origin"]):
                return edge["ticket_price"] + self.heuristic(e, edges)
        return edge["ticket_price"]

    def shortest_path(self, origin, category, budget):
        paths = []
        emin = None
        pathmin = 0
        visited = {}
        cur = origin
        prev = None
        edges = self.get_all_edges(category)
        edges = sorted(edges, key=lambda k: k['ticket_price'])
        prev_paths_length = -1
        paths_length = 0
        while budget - pathmin > 0 and prev_paths_length != paths_length:
            for edge in edges:
                print(visited, edge["destination"])
                if (edge["origin"] == cur):
                    if edge["destination"] in visited:
                        continue
                    elif (emin is None):
                        emin = edge
                        pathmin = self.heuristic(emin, paths)
                    elif self.heuristic(edge, paths) <= pathmin:
                        emin = edge
                        pathmin = self.heuristic(emin, paths)
            prev = cur
            if emin is None:
                break
            prev_paths_length = paths_length
            if emin not in paths:
                paths.append(emin)
                paths_length = len(paths)
            cur = emin["destination"]
            visited[cur] = True
            visited[prev] = True
            emin = None
        if (budget - pathmin < 0):
            return paths[0:-1]
        else:
            return paths
