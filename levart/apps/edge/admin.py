from django.contrib import admin

from levart.utils.loading import get_model

City = get_model('edge', 'Edge')


@admin.register(City)
class EdgeAdmin(admin.ModelAdmin):
    list_display = ['source', 'destination',
                    'method', 'ticket_price', 'distance', 'straight_line_distance']
    search_fields = ['source', ]
