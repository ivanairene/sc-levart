from django.db import models
from django.utils.translation import ugettext_lazy as _

from levart.utils.loading import get_model

City = get_model('city', 'City')


class Edge(models.Model):
    source = models.ForeignKey(
        City, related_name='edge_sources', on_delete=models.PROTECT)
    destination = models.ForeignKey(
        City, related_name='edge_destinations', on_delete=models.PROTECT)
    method = models.CharField(_('method'),
                              max_length=15,
                              default='pesawat',
                              choices=(
        ('pesawat', 'pesawat'),
        ('kereta_api', 'kereta_api')
    )
    )
    ticket_price = models.IntegerField(default=0)
    distance = models.IntegerField(default=0)
    straight_line_distance = models.IntegerField(default=0)
    ticket_page_url = models.URLField(max_length=200, null=True, blank=True)

    def __str__(self):
        return str(self.source.name)

    class Meta:
        app_label = 'edge'
        verbose_name = _('Edge')
