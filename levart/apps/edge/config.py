from django.apps import AppConfig


class EdgeConfig(AppConfig):
    name = 'levart.apps.edge'
