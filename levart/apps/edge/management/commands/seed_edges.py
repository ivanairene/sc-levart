import json

from django.core.management.base import BaseCommand

from levart.utils.loading import get_model

Edge = get_model('edge', 'Edge')
City = get_model('city', 'City')


class Command(BaseCommand):

    def handle(self, *args, **options):
        Edge.objects.all().delete()
        with open('data.json') as f:
            data = json.load(f)
            for key in data:
                edge = data[key][0]
                try:
                    source = City.objects.get(name=edge["source"])
                    destination = City.objects.get(name=edge["destination"])
                    ticket_price = int(edge["ticket_price"].replace(
                        "IDR", "").replace(".", "").strip())
                    # print("Creating edges for source ", source.name,
                    #     " and destination ", destination.name)
                    Edge.objects.create(
                        source=source,
                        destination=destination,
                        ticket_price=ticket_price,
                        method=edge["method"],
                        distance=edge["distance"],
                        straight_line_distance=edge["straight_line_distance"],
                        ticket_page_url=edge["ticket_page_url"]
                    )
                    # print("Success creating edges")
                except Exception as e:
                    print("Error!", edge["source"], edge["destination"])
