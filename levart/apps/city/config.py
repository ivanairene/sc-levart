from django.apps import AppConfig


class CityConfig(AppConfig):
    name = 'levart.apps.city'
