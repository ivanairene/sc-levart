from django.contrib import admin

from levart.utils.loading import get_model

City = get_model('city', 'City')


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ['name', 'city_view',
                    'beach_view', 'mountain_view', 'culinary']
    search_fields = ['name', ]
