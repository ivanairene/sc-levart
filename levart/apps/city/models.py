from django.db import models
from django.utils.translation import ugettext_lazy as _


class City(models.Model):
    name = models.CharField(_('Name'), max_length=128)
    city_view = models.BooleanField(_('City View'), default=False)
    beach_view = models.BooleanField(_('Beach View'), default=False)
    mountain_view = models.BooleanField(_('Mountain View'), default=False)
    culinary = models.BooleanField(_('Culinary'), default=False)

    def __str__(self):
        return str(self.name)

    class Meta:
        app_label = 'city'
        verbose_name = _('City')
