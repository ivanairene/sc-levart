from levart.utils.loading import get_model

Edge = get_model('edge', 'Edge')
City = get_model('city', 'City')


def get_all_edges(self):
    edges = []
    for edge in Edge.objects.all():
        edges.append({
            "source": edge.source.name,
            "destination": edge.destination.name,
            "method": edge.method,
            "ticket_price": edge.ticket_price,
            "ticket_page_url": edge.ticket_page_url
        })
    return edges


def heuristic(edge, edges):
    for e in edges:
        if (e["destination"] == edge["source"]):
            return e["ticket_price"] + heuristic(e, edges)


def shortest_path(self, source, budget):
    paths = []
    emin = None
    pathmin = None
    visited = []
    cur = source
    # prev = None
    edges = get_all_edges()
    while (budget - pathmin > 0) or cur is not None:
        for edge in edges:
            if (edge["source"] == cur):
                if (emin is None):
                    emin = edge
                    pathmin = heuristic(emin, edges)
                elif edge["destination"] in visited:
                    continue
                elif heuristic(edge, edges) <= pathmin:
                    emin = edge
                    pathmin = heuristic(emin, edges)
        # prev = cur
        if emin not in paths:
            paths.append(emin)
        cur = emin["destination"]
        visited.append(cur)
    if (budget - pathmin < 0):
        return paths[0:-1]
    else:
        return paths
