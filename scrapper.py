import mechanize
import urllib2
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchAttributeException
from selenium.webdriver.common.keys import Keys
import os
import re
import datetime
from bs4 import BeautifulSoup
import requests
import json

def main(awal, akhir):
	browser.get("https://tiket.com/pesawat")

	elem = browser.find_elements_by_class_name("input-airport")
	elem_from = elem[0].clear()
	elem_from = elem[0]
	elem_from.send_keys(awal)
	
	elem_to = elem[1].clear()
	elem_to = elem[1]
	elem_to.send_keys(akhir)
	res = browser.execute_script("return document.documentElement.outerHTML")
	soup = BeautifulSoup(res, 'lxml')

	box = soup.find_all('div', {'class': 'item'})
	origin = [item for item in box if awal in item.get_text().lower() and item.find('i', {'class': 'tixicon-plane'})][0]
	destination = [item for item in box if akhir in item.get_text().lower() and item.find('i', {'class': 'tixicon-plane'})][0]

	from_code = origin.find("div", class_="col-code").get_text()
	to_code = destination.find("div", class_="col-code").get_text()


	date = str(datetime.date.today() + datetime.timedelta(days=1))
	print(date)

	url = 'https://www.tiket.com/pesawat/search?d=' + from_code + '&a=' + to_code + '&dType=AIRPORT&aType=AIRPORT&date=' + date + '&adult=1&child=0&infant=0&class=economy'
	print(url)

	browser.get(url)
	time.sleep(5)
	res = browser.execute_script("return document.documentElement.outerHTML")

	soup = BeautifulSoup(res, 'lxml')

	flight = soup.find("div", class_="root-flight-list")
	if flight != None:
		price = flight.find("div", class_="text-price").get_text()
		print(flight.get_text())
		print(price)
	else:
		print("Flight isn't available")
		price = 0
	return [{"source":awal.title(), "destination":akhir.title(), "method":"pesawat", 
			"ticket_price":price, "distance":0, "straight_line_distance":0,
			"ticket_page_url":url}]


cities = ['banda aceh', 'medan', 'padang', 'pekanbaru', 'tanjung pinang', 'jambi', 'bengkulu',
			'palembang', 'pangkalpinang', 'bandar lampung', 'bandung', 'jakarta', 'semarang',
			'yogyakarta', 'surabaya', 'denpasar', 'kupang', 'tanjung selor', 'pontianak',
			'palangkaraya', 'banjarmasin', 'samarinda', 'gorontalo', 'manado', 'mamuju', 'palu',
			'makassar', 'kendari', 'ambon', 'manokwari', 'jayapura']

br = mechanize.Browser()
br.set_handle_robots(False)
br.set_handle_refresh(False)
br.addheaders = [('User-agent', 'Firefox')]

browser = webdriver.Firefox()

data = {}
counter = 0

for x in cities:
	for y in cities:
		if x != y:
			value = main(x,y)
			if value[0]["ticket_price"] != 0:
				data["edge"+str(counter)] = value
				counter += 1
				print(value)

with open('data2.json', 'w') as outfile:
	json.dump(data, outfile, indent=4)

browser.quit()